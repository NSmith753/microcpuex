import javax.swing.*;



/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.3.11
 * @since		2015-07-19
 * 
 */
 public class HelperLib
 {
	/**
	 * Swaps the values of the two specified parameters and returns the result.
	 *
	 * @param num1 first specified number for the swap
	 * @param num2 second specified number for the swap
	 * @return swapResult array containing the swapped values
	 *
	 */ 
	public int[] swap(int num1, int num2)
	{
		final int FIRST_NUM = 0; // constant holding the array subscript location for the first number after the swap
		final int SECOND_NUM = 1; // constant holding the array subscript location for the second number after the swap
		
		int temp = 0;
		int[] swapResult = new int[2]; // holds the results of the swap for returning to the calling function
		
		// swap the values in num1 and num2
		temp = num1;
		num1 = num2;
		num2 = temp;
		
		// store the result in the swapResult array
		swapResult[FIRST_NUM] = num1;
		swapResult[SECOND_NUM] = num2;
		
		return swapResult;
		
	} // end of swap()
	
	
	/**
	 * Prompts the user for data input using an input dialog box.
	 *
	 * @param message the user prompt that will be displayed to the user
	 * @param title the title of the input dialog box
	 * @param optionPaneType the type of option pane for the input dialog box
	 * @return user input data string
	 *
	 */
	public String getInput(String message, String title, int optionPaneType)
	{
		return JOptionPane.showInputDialog(null, message, title, optionPaneType);
				
	} // end of getInput()
	
	
	/**
	 * Prompts the user for data input using an input dialog box as a question message option pane.
	 *
	 * @param message the user prompt that will be displayed to the user
	 * @param title the title of the input dialog box
	 * @return user input data string
	 *
	 */
	public String getInput(String message, String title)
	{
		return JOptionPane.showInputDialog(null, message, title, JOptionPane.QUESTION_MESSAGE);
				
	} // end of getInput()
	
	
	/**
	 * Displays message box to the user with the provided information.
	 *
	 * @param message the user prompt that will be displayed to the user
	 * @param title the title of the input dialog box
	 * @param optionPaneType the type of option pane for the input dialog box
	 * @return void
	 *
	 */
	public void displayMsg(String message, String title, int optionPaneType)
	{
		JOptionPane.showMessageDialog(null, message, title, optionPaneType);
		
	} // end of displayMsg()
	
	
	/**
	 * Displays message box to the user with the provided information as an information message option pane.
	 *
	 * @param message the user prompt that will be displayed to the user
	 * @param title the title of the input dialog box
	 * @return void
	 *
	 */
	public void displayMsg(String message, String title)
	{
		JOptionPane.showMessageDialog(null, message, title, JOptionPane.INFORMATION_MESSAGE);
		
	} // end of displayMsg()
	

	public int getOptions(String message, String title, int optionType, int optionPaneType, Object[] options, String initialValue)
	{
		return JOptionPane.showOptionDialog(null, message, title, optionType, optionPaneType, null, options, initialValue);
		
	}
	

	public int getFullOptions(String message, String title, Object[] options, String initialValue)
	{
		return getOptions(message, title, JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, options, initialValue);
		
	}
	
	
	/**
	 * Prompts the user for data input using an input dialog box.
	 *
	 * @param message the user prompt that will be displayed to the user
	 * @param title the title of the input dialog box
	 * @param optionPaneType the type of option pane for the input dialog box
	 * @return user input data string converted to an integer or exits the program
	 *
	 */
	public int getNumberInput(String message, String title, int optionPaneType)
	{
		boolean isValid = true; // tracks the validation of the user input and acts as the control condition for the user input prompt loop
		String userInput = ""; // holds the user input from the prompt
		int convertedUserInput = 0; // holds the converted user input from string to integer
		
		
		do
		{
			userInput = getInput(message, title, optionPaneType); // get user input
			
			if (userInput == null)
			{
				System.out.println("The user cancelled the data entry process.");
				displayMsg("Data entry process cancelled.", "Data Entry", JOptionPane.INFORMATION_MESSAGE);
				
				System.exit(0); // terminate program since continuing further would propagate errors throughout the system.
			}
			
			
			if (userInput == "")
			{
				displayMsg("Invalid data entry, please enter a valid integer.", "Data Entry", JOptionPane.INFORMATION_MESSAGE);
				isValid = false; // set loop to prompt for correct input
			}
			
			
			try
			{
				// attempt to convert the user input string to an integer
				convertedUserInput = Integer.parseInt(userInput);
				isValid = true; // set loop to stop checking input
				
			} // end of try statement
			catch(NumberFormatException e) 
			{
				// the input dialog returned a value other than an integer number in the form of a string
				
				System.out.println("Invalid data entry, please enter a valid integer.");
				displayMsg("Invalid data entry, please enter a valid integer.", "Data Entry", JOptionPane.INFORMATION_MESSAGE);
				isValid = false; // set loop to prompt for correct input
				
			} // end of NumberFormatException e
			
		}
		while (! isValid); // end of do While
				
		
		return convertedUserInput; // return the input from the user converted to an integer
		
	} // end of getNumberInput()
	
	
	/**
	 * Displays the summary of the specified value in a message box.
	 * 
	 * @param ordinal the ordinal values (first, second, fifth, tenth) to be used as string literals when displaying the summaries
	 * @param value the value to be displayed in the summary
	 * @result void
	 *
	 */
	public void displaySummary(String ordinal, int value)
	{
		String outputMsg = "The new value of " + ordinal.toLowerCase().trim() + " number is: " + value;	// build output string	
		
		displayMsg(outputMsg, "Summary", JOptionPane.INFORMATION_MESSAGE); // display output string as a message box
		
	} // end of displaySummary()
	
	
	
	public String padString(String value, String paddingSide, int paddingAmount, String paddingStr)
	{
		String output = value;
		
		paddingSide = paddingSide.toLowerCase();
		
		for (int paddingCount = 0; paddingCount < paddingAmount; paddingCount++)
		{
			if (paddingSide == "pre")
			{
				output = paddingStr + output;
			}
			else if (paddingSide == "post")
			{
				output += paddingStr;
			}
			
		}
		
		return output;
		
	}
	
	
 } // end of HelperLib class