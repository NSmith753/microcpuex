

/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-09-12
 * 
 */

public class InstructionParser
{
	public final int VALID_INSTRUCTION_LENGTH = 3;
	
	private InputValidator inputValidator = new InputValidator();
	
	
	public String[] parseInstructionSetStr(String instructionSetStr, String separator)
	{
		return instructionSetStr.split(separator);
			
	}
	
	public boolean validateInstructionSet(String[] parsedInstructionSet)
	{
		boolean isValid = true;
		
		for (int instructionCount = 0; instructionCount < parsedInstructionSet.length && isValid; instructionCount++)
		{
			if (! isValidInstruction(parsedInstructionSet[instructionCount]))
			{
				isValid = false;
			}
		}
		
		return isValid;
		
	}
	
	public boolean isValidInstructionSet(String instructionSetStr, String separator)
	{
		String[] parsedInstructionSet = parseInstructionSetStr(instructionSetStr, separator);
		
		return validateInstructionSet(parsedInstructionSet);
		
	}
	
	public boolean isValidInstruction(String instructionStr)
	{		
		boolean isValid = true;
		char instructionPart = ' ';
		
		
		if (instructionStr.length() != this.VALID_INSTRUCTION_LENGTH)
		{			
			if (this.inputValidator.isNumeric(instructionStr))
			{
				for (int count = 0; count < this.VALID_INSTRUCTION_LENGTH; count++)
				{
					instructionPart = instructionStr.charAt(count);
					
					switch(count)
					{
						case 0: 
							if (! isValidInstructionCode(instructionPart))
							{
								isValid = false;
							}
							
							break;
							
						case 1: 
							if (! isValidRegisterID(instructionPart))
							{
								isValid = false;
							}
							
							break;
							
						case 2: 
							if (! isValidInstructionValue(instructionPart))
							{
								isValid = false;
							}
							
							break;
													
					}
					
				}
				
			}
			
		}
		
		
		return isValid;
		
	}
	
	public boolean isValidInstructionCode(char instructionCode)
	{
		boolean isValid = false;
		int convertedInstructionCode = (int)instructionCode;
		
		
		if (convertedInstructionCode >= 0 && convertedInstructionCode <= 9)
		{
			isValid = true;
		}
		
		return isValid;
	}
	
	public boolean isValidRegisterID(char registerID)
	{
		boolean isValid = false;
		int convertedRegisterID = (int)registerID;
		
		
		if (convertedRegisterID >= 0 && convertedRegisterID <= 9)
		{
			isValid = true;
		}
		
		return isValid;
	}
	
	public boolean isValidInstructionValue(char instructionValue)
	{
		boolean isValid = false;
		int convertedInstructionValue = (int)instructionValue;
		
		
		if (convertedInstructionValue >= 0 && convertedInstructionValue <= 9)
		{
			isValid = true;
		}
		
		return isValid;
	}
	
	public Instruction[] buildInstructionSequence(String[] parsedInstructionSet)
	{
		String instructionStr = "";
		char instructionPart = ' ';
		
		String instructionType = "";
		String instructionRegisterID = "";
		int instructionValue = 0;
		
		Instruction[] instructionSequence = new Instruction[parsedInstructionSet.length];
		
		
		if (validateInstructionSet(parsedInstructionSet))
		{
			for (int instructionCount = 0; instructionCount < parsedInstructionSet.length; instructionCount++)
			{
				instructionStr = parsedInstructionSet[instructionCount];
				
				for (int count = 0; count < this.VALID_INSTRUCTION_LENGTH; count++)
				{					
					instructionPart = instructionStr.charAt(count);
					
					switch(count)
					{
						case 0: 
							instructionType = String.valueOf(instructionPart);
							
							break;
							
						case 1: 
							instructionRegisterID = String.valueOf(instructionPart);
							
							break;
							
						case 2: 
							instructionValue = Integer.parseInt(String.valueOf(instructionPart));
							
							break;
													
					}
					
				}
				
				instructionSequence[instructionCount] = new Instruction(instructionType, instructionRegisterID, instructionValue);
								
			}			
			
		}
		
		
		return instructionSequence;
		
	}

	public int convertToStorageFormat(int value)
	{
		return value % 1000;
	}
	
}