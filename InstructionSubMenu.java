import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-09-19
 * 
 */

public class InstructionSubMenu extends MicroCPUexMenuFrame
{
	public InstructionSubMenu()
	{
		super();
	}
	
	public InstructionSubMenu(String menuName, String frameTitle, int width, int height)
	{
		super(menuName, frameTitle, width, height);
	}	
			
	public void build()
	{
		getHeaderLabel().setText("Add instruction set for processing");
		getControlsLabel().setText("Please select an action");
		
		JButton addInstructionButton = new JButton("Add Instruction");
		JButton backButton = new JButton("Back");
		
		addInstructionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				getMicroCPUexCore().process();
				
			}
		});
		
		backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				getMicroCPUexGUI().getMenu("main").show();				
				close();
				
			}
		});
		
				
		
		getControlPanel().add(addInstructionButton);
		getControlPanel().add(backButton);
				
	}
}