import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-09-19
 * 
 */

public class MainMenuFrame extends MicroCPUexMenuFrame
{	
	public MainMenuFrame()
	{
		super();
	}
	
	public MainMenuFrame(String menuName, String frameTitle, int width, int height)
	{
		super(menuName, frameTitle, width, height);
	}
				
	public void build()
	{
		getHeaderLabel().setText("Welcome to microCPUex");
		getControlsLabel().setText("Please select an action");
		
		JButton addInstructionButton = new JButton("Add Instruction");
		JButton viewRegistersButton = new JButton("View Registers");
		JButton quitButton = new JButton("Quit");
				
		addInstructionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				getMicroCPUexGUI().getMenu("instruction-sub").show();				
				close();
				
			}
		});
		
		viewRegistersButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				getMicroCPUexGUI().getMenu("register-sub").show();				
				close();
				
			}
		});
		
		quitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		
		getControlPanel().add(addInstructionButton);
		getControlPanel().add(viewRegistersButton);
		getControlPanel().add(quitButton);
				
	}
}