import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-09-21
 * 
 */

public abstract class MenuFrameAbstract
{	
	private String menuName;
	private JFrame menuFrame;
	private JLabel headerLabel;
	private JLabel controlsLabel;
	private JPanel controlPanel;
	

	public MenuFrameAbstract()
	{
		prepareGUI("Menu Name", "App", 400, 600);
	}
	
	public MenuFrameAbstract(String menuName, String frameTitle, int width, int height)
	{
		prepareGUI(menuName, frameTitle, width, height);
	}

	private void prepareGUI(String menuName, String frameTitle, int width, int height)
	{
		this.menuName = menuName;
		
		menuFrame = new JFrame(frameTitle);
		menuFrame.setSize(width, height);
		menuFrame.setLayout(new GridLayout(3, 1));
		
		menuFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent) {
				System.exit(0);
			}
		});
		
		
		headerLabel = new JLabel("", JLabel.CENTER);
		controlsLabel = new JLabel("", JLabel.CENTER);
		
		controlsLabel.setSize(350, 100);
		
		controlPanel = new JPanel();
		controlPanel.setLayout(new FlowLayout());
		
		menuFrame.add(headerLabel);
		menuFrame.add(controlsLabel);
		menuFrame.add(controlPanel);
	}
	
	public String getMenuName()
	{
		return menuName;
	}
	
	public JFrame getMenuFrame()
	{
		return menuFrame;
	} 
	
	public JLabel getHeaderLabel()
	{
		return headerLabel;
	}
	
	public JLabel getControlsLabel()
	{
		return controlsLabel;
	}
	
	public JPanel getControlPanel()
	{
		return controlPanel;
	}
	
	
	public void setMenuName(String menuName)
	{
		this.menuName = menuName;
	}
	
	public void setMenuFrame(JFrame menuFrame)
	{
		this.menuFrame = menuFrame;
	}
	
	public void setHeaderLabel(JLabel headerLabel)
	{
		this.headerLabel = headerLabel;
	}
	
	public void setControlsLabel(JLabel controlsLabel)
	{
		this.controlsLabel = controlsLabel;
	}
	
	public void setControlPanel(JPanel controlPanel)
	{
		this.controlPanel = controlPanel;
	}
		
	public void show()
	{
		getMenuFrame().setVisible(true);
	}
	
	public void hide()
	{
		getMenuFrame().setVisible(false);
	}
	
	public void close()
	{
		getMenuFrame().dispose();
	}
	
	
	public abstract void build();
	
	
}