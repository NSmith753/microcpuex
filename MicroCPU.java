

/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.7.5.13
 * @since		2015-09-12
 * 
 */

public class MicroCPU
{
	private boolean canProcessInstructions;
	private boolean terminateExecution;
	private boolean instructionError;
	private String instructionErrorStr;
	
	private InstructionParser instructionParser;
	private RegisterList registerList;
	private HelperLib helperLib;
	
	public MicroCPU()
	{
		this.canProcessInstructions = true;
		this.terminateExecution = false;
		this.instructionError = false;
		this.instructionErrorStr = "";
		
		instructionParser = new InstructionParser();
		registerList = new RegisterList();
		helperLib = new HelperLib();
		
	}
	
	public MicroCPU(boolean canProcessInstructions, boolean terminateExecution, boolean instructionError, String instructionErrorStr)
	{
		this.canProcessInstructions = canProcessInstructions;
		this.terminateExecution = terminateExecution;
		this.instructionError = instructionError;
		this.instructionErrorStr = instructionErrorStr;
		
		instructionParser = new InstructionParser();
		registerList = new RegisterList();
		helperLib = new HelperLib();
		
	}
	
	public boolean getCanProcessInstructions()
	{
		return this.canProcessInstructions;
	
	}
	
	public boolean getTerminateExecution()
	{
		return this.terminateExecution;
	
	}
	
	public boolean getInstructionError()
	{
		return this.instructionError;
	
	}
	
	public String getInstructionErrorStr()
	{
		return this.instructionErrorStr;
	
	}
	
	public InstructionParser getInstructionParser()
	{
		return this.instructionParser;
		
	}
	
	public RegisterList getRegisterList()
	{
		return this.registerList;
		
	}
	
	public HelperLib getHelperLib()
	{
		return this.helperLib;
		
	}
	
	
	public void setCanProcessInstructions(boolean canProcessInstructions)
	{
		this.canProcessInstructions = canProcessInstructions;
		
	}
	
	public void setTerminateExecution(boolean terminateExecution)
	{
		this.terminateExecution = terminateExecution;
		
	}
	
	public void setInstructionError(boolean instructionError)
	{
		this.instructionError = instructionError;
		
	}
	
	public void setInstructionErrorStr(String instructionErrorStr)
	{
		this.instructionErrorStr = instructionErrorStr;
		
	}
	
	public void setInstructionParser(InstructionParser instructionParser)
	{
		this.instructionParser = instructionParser;
		
	}
	
	public void setRegisterList(RegisterList registerList)
	{
		this.registerList = registerList;
		
	}
	
	public void setHelperLib(HelperLib helperLib)
	{
		this.helperLib = helperLib;
		
	}
		
	public void resetMicroCPU()
	{
		this.canProcessInstructions = true;
		this.terminateExecution = false;
		this.instructionError = false;
		this.instructionErrorStr = "";
	}
	
		
	public boolean executeInstruction(Instruction instruction)
	{
		int calculationResult = 0;
				
				
		switch (instruction.getType())
		{
			case "1":
				this.terminateExecution = true;
				
				break;
				
			case "2":
				calculationResult = instruction.getValue();
				
				break;
			
			case "3":
				calculationResult = this.registerList.getRegisterValue(instruction.getRegisterID()) + instruction.getValue();
				
				break;
	
			case "4":
				calculationResult = this.registerList.getRegisterValue(instruction.getRegisterID()) * instruction.getValue();
				
				break;
			
			case "5":
				calculationResult = this.registerList.getRegisterValue(instruction.getRegisterID()) / instruction.getValue();
				
				break;
			
			case "6":
				calculationResult = this.registerList.getRegisterValue(String.valueOf(instruction.getValue()));
				
				break;
			
			case "7":
				calculationResult = this.registerList.getRegisterValue(instruction.getRegisterID()) + this.registerList.getRegisterValue(String.valueOf(instruction.getValue()));
				
				break;
			
			case "8":
				calculationResult = (int) Math.sqrt(this.registerList.getRegisterValue(instruction.getRegisterID()));
				
				break;
			
			case "9":
				calculationResult = this.registerList.getRegisterValue(instruction.getRegisterID()) ^ instruction.getValue();
				
				break;
			
			case "0":
				this.instructionErrorStr = instruction.getType() + instruction.getRegisterID() + instruction.getValue();
				instructionError = true;
				
				break;
			
			default:
				
		}
		
		
		return this.registerList.updateRegister(instruction.getRegisterID(), this.instructionParser.convertToStorageFormat(calculationResult));
		
	}
	
	public void buildRegisterList()
	{
		final int TOTAL_REGISTERCOUNT = 10;
		final int DEFAULT_REGISTER_VALUE = 0;
		final int REGISTER_MAXVALUE = 999;
		final int REGISTER_MINVALUE = 0;
		
		this.registerList.removeAll();
		
		for (int registerCount = 0; registerCount < TOTAL_REGISTERCOUNT; registerCount++)
		{
			this.registerList.add(String.valueOf(registerCount), DEFAULT_REGISTER_VALUE, REGISTER_MINVALUE, REGISTER_MAXVALUE);
		}
		
	}

	public void process()
	{
		int instructionCount = 0;
		String instructionSetStr = getInstructionSet();
		Instruction[] instructionSequence = instructionParser.buildInstructionSequence(instructionParser.parseInstructionSetStr(instructionSetStr, " "));
		
		buildRegisterList();
		
		do
		{						
			executeInstruction(instructionSequence[instructionCount]);
			
			if (this.instructionError)
			{
				this.canProcessInstructions = false;				
			}
			else
			{			
				instructionCount++;
			}
			
			if (instructionCount >= instructionSequence.length)
			{
				this.canProcessInstructions = false;
			}
			
			display();
						
		}
		while(this.canProcessInstructions);
		
		resetMicroCPU();
				
	}
	
	/**
	 * Generates user prompts based on the selected lookup value.
	 *
	 * @param lookupValue used to specify which user prompt is generated
	 * @return the value from the helperLib.getInput()
	 *
	 */ 
	public String promptUser(String lookupValue)
	{
		String title = "";
		String message = "";
		
		lookupValue = lookupValue.toLowerCase();
		
		switch (lookupValue)
		{
			case "instruction-set":
				title = "Instruction Set";
				message = "Please enter the instruction set as a continuous string where a triplet value represents an instruction. (e.g. 356 866 734 724)";
				
				break;					
				
			default:
				title = "Data Entry";
				message = "Please enter data.";
		}
		
		return this.helperLib.getInput(message, title);
	}
	
	
	/**
	 * Prompts user for the principal amount until valid input is entered or the user cancels data entry.
	 * 
	 * @return inputData the valid input data
	 *
	 */ 
	public String getInstructionSet()
	{
		String inputData = "";
		
		do
		{
			inputData = this.promptUser("instruction-set");
			
		}
		while (! validateInput(inputData, "instruction-set"));
		
		
		return inputData;
		
	}
	
	
	
	/**
	 * Validates the input data based on the specified data type.
	 *
	 * @param inputData the data that needs to be validated
	 * @param expectedDataType the data type the input data is expected to be
	 * @return isValid true if the input data passes validation otherwise false
	 *
	 */ 
	public boolean validateInput(String inputData, String expectedDataType)
	{
		boolean isValid = true; // tracks the validation of the user input
		String userInput = ""; // holds the user input from the prompt
		
		expectedDataType = expectedDataType.toLowerCase();
		
		if (inputData == null)
		{
			System.out.println("The user cancelled the data entry process.");
			this.helperLib.displayMsg("Data entry process cancelled.", "Data Entry");
			
			System.exit(0); // terminate program since continuing further would propagate errors throughout the system.
		}
		
		
		if (inputData.compareTo("") == 0)
		{
			this.helperLib.displayMsg("Invalid data entry, please enter a valid instruction code.", "Data Entry");
			isValid = false;
		}
		
		if (isValid)
		{
			switch (expectedDataType)
			{
				case "instruction-set":
					if (! instructionParser.isValidInstructionSet(inputData, " "))
					{
						isValid = false;
						this.helperLib.displayMsg("Invalid data entry, please enter a valid instruction code.", "Data Entry");
					}
					
					break;
					
				default:
					isValid = false;
			}
		}
		
		
		return isValid;		
	} // end of validateInput()
	
	
	public void display()
	{
		String title = "Summary";
		String message = "";
		
		String registerIDStr = "";
		String registerValueStr = "";
		
		
		if (this.instructionError)
		{
			message = "Instruction code '" + this.instructionErrorStr + "' is not supported by the system.";
			this.instructionErrorStr = "";
			this.instructionError = false;
			
		}
		else if (this.terminateExecution)
		{
			message = "Program Terminated.";
		}
		else
		{
			if (this.registerList.getSize() > 0)
			{
				for (int registerCount = 0; registerCount < this.registerList.getSize(); registerCount++)
				{
					registerIDStr = this.registerList.getRegister(registerCount).getID();
					registerValueStr = String.valueOf(this.registerList.getRegister(registerCount).getValue());
					
					message += registerIDStr + ": " + formatPaddedRegisterValue(registerValueStr);
					
					if (registerCount + 1 < this.registerList.getSize())
					{
						message += ", ";
					}
					
				}
				
			}
			else
			{
				message = "There are no registers listed in the MicroCPU.";
			}
		}
		
		
		helperLib.displayMsg(message, title);
		
	}
	
	
	public String formatPaddedRegisterValue(String value)
	{
		final int OUTPUTSTR_FORMATTED_LENGTH = 3;
		
		int paddingAmount = OUTPUTSTR_FORMATTED_LENGTH - value.length();
		
		return helperLib.padString(value, "pre", paddingAmount, "0");
		
	}
	
	
}