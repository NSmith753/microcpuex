import java.awt.event.*;


/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-09-17
 * 
 */

public class MicroCPUex
{	
	public void launch()
	{
		MainMenuFrame splashPage = new MainMenuFrame("splash", "microCPUex", 400, 600);		
		splashPage.buildGUI();
		
		splashPage.getMicroCPUexGUI().getMenu("main").show();
	}
}

