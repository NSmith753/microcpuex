

/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-09-23
 * 
 */
 
 public class MicroCPUexCore extends MicroCPU
 {
	 private boolean registersIsBuilt = false;
	 private boolean canDisplayRegistersStatusWhileProcessing = true;
	 private boolean canGetUserInputData = true;
	 
	 
	 public MicroCPUexCore()
	 {
		 super();
	 }
	 
	 public MicroCPUexCore(boolean canProcessInstructions, boolean terminateExecution, boolean instructionError, String instructionErrorStr)
	 {
		 super(canProcessInstructions, terminateExecution, instructionError, instructionErrorStr);
	 }
	 
	 
	 public boolean getRegisterIsBuilt()
	 {
		 return this.registersIsBuilt;
		 
	 }
	 
	 public boolean getCanDisplayRegistersStatusWhileProcessing()
	 {
		 return this.canDisplayRegistersStatusWhileProcessing;
		 
	 }
	 
	 public boolean getCanGetUserInputData()
	 {
		 return this.canGetUserInputData;
		 
	 }
	 
	 public void setRegisterIsBuilt(boolean registersIsBuilt)
	 {
		 this.registersIsBuilt = registersIsBuilt;
		 
	 }
	 
	 public void setCanDisplayRegistersStatusWhileProcessing(boolean canDisplayRegistersStatusWhileProcessing)
	 {
		 this.canDisplayRegistersStatusWhileProcessing = canDisplayRegistersStatusWhileProcessing;
		 
	 }
	 
	 public void setCanGetUserInputData(boolean canGetUserInputData)
	 {
		 this.canGetUserInputData = canGetUserInputData;
		 
	 }
	 
	 
	 public void clearRegisters()
	 {
		 String message = "All registers cleared successfully.";
		 String title = "Operation";
		 
		 getRegisterList().removeAll();
		 buildRegisterList();
		 setRegisterIsBuilt(true);
		 
		 getHelperLib().displayMsg(message, title);
		 
	 }
	 
	 /**
	 * @override
	 *
	 */
	 public void process()
	 {
		 int instructionCount = 0;
		 String instructionSetStr = getInstructionSet();
		 Instruction[] instructionSequence = getInstructionParser().buildInstructionSequence(getInstructionParser().parseInstructionSetStr(instructionSetStr, " "));
		 
		 if (! getRegisterIsBuilt())
		 {			 
			buildRegisterList();
			setRegisterIsBuilt(true);
			
		 }
		 
		 
		 do
		 {						
			 executeInstruction(instructionSequence[instructionCount]);
			 
			 if (getInstructionError())
			 {
				 setCanProcessInstructions(false);	
				 
			 }
			 else
			 {			
				 instructionCount++;
				 
			 }
			
			 if (instructionCount >= instructionSequence.length)
			 {
				 setCanProcessInstructions(false);
				 
			 }
			 
			 
			 if (getCanDisplayRegistersStatusWhileProcessing())
			 {
				display();
				
			 }
						
		 }
		 while(getCanProcessInstructions());
		
		 resetMicroCPU();
	 }
	 
	 /**
	 * Prompts user for the principal amount until valid input is entered or the user cancels data entry.
	 * 
	 * @return inputData the valid input data
	 *
	 */ 
	public String getInstructionSet()
	{
		boolean isValid = false;
		String inputData = "";
		
		
		do
		{
			inputData = this.promptUser("instruction-set");
			isValid = validateInput(inputData, "instruction-set");
			
			if (getCanGetUserInputData())
			{
				if (isValid)
				{
					setCanGetUserInputData(false);
				}
				
			}
			else
			{
				inputData = "100"; // set the cpu to terminate gracefully with a valid instruction
			}
			
		}
		while (getCanGetUserInputData());
		
		setCanGetUserInputData(true);
		
		return inputData;
		
	}
	 
	 /**
	 * Validates the input data based on the specified data type.
	 *
	 * @override
	 *
	 * @param inputData the data that needs to be validated
	 * @param expectedDataType the data type the input data is expected to be
	 * @return isValid true if the input data passes validation otherwise false
	 *
	 */ 
	public boolean validateInput(String inputData, String expectedDataType)
	{
		boolean isValid = true; // tracks the validation of the user input
		String userInput = ""; // holds the user input from the prompt
		
		expectedDataType = expectedDataType.toLowerCase();
		
		if (inputData == null)
		{
			System.out.println("The user cancelled the data entry process.");
			getHelperLib().displayMsg("Data entry process cancelled.", "Data Entry");
			isValid = false;
			setCanGetUserInputData(isValid);
			
		}		
		else if (inputData.compareTo("") == 0)
		{
			getHelperLib().displayMsg("Invalid data entry, please enter a valid instruction code.", "Data Entry");
			isValid = false;
			
		}
		
		
		if (isValid)
		{
			switch (expectedDataType)
			{
				case "instruction-set":
					if (! getInstructionParser().isValidInstructionSet(inputData, " "))
					{
						isValid = false;
						getHelperLib().displayMsg("Invalid data entry, please enter a valid instruction code.", "Data Entry");
					}
					
					break;
					
				default:
					isValid = false;
			}
		}		
		
		return isValid;		
	} // end of validateInput()
	 
	 /**
	 * @override
	 *
	 */
	 
	 public void display()
	 {
		 String title = "Summary";
		 String message = "";
		 
		 String registerIDStr = "";
		 String registerValueStr = "";
		 
		 
		 if (getInstructionError())
		 {
		 	 message = "Instruction code '" + getInstructionErrorStr() + "' is not supported by the system.";
			 setInstructionErrorStr("");
			 setInstructionError(false);
			
		 }
		 else if (getTerminateExecution())
		 {
			 message = "Processing Complete.";
		 }
		 else
		 {
			 if (getRegisterList().getSize() > 0)
			 {
				 for (int registerCount = 0; registerCount < getRegisterList().getSize(); registerCount++)
				 {
					 registerIDStr = getRegisterList().getRegister(registerCount).getID();
					 registerValueStr = String.valueOf(getRegisterList().getRegister(registerCount).getValue());
					 
					 message += registerIDStr + ": " + formatPaddedRegisterValue(registerValueStr);
					 
					 if (registerCount + 1 < getRegisterList().getSize())
					 {
						 message += ", ";
					 }
					
				 }
				
			 }
			 else
			 {
				 message = "There are no registers listed in the MicroCPUex.";
			 }
		 }
		
		
		 getHelperLib().displayMsg(message, title);
		 
	 }
	 
	 
 }