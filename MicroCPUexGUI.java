import java.util.*;


/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-09-21
 * 
 */

public class MicroCPUexGUI
{
	private List<MenuFrameAbstract> menuList = new ArrayList<MenuFrameAbstract>();
	
	
	public void setMenuList(List<MenuFrameAbstract> menuList)
	{
		this.menuList = menuList;
	}
	
	public List<MenuFrameAbstract> getMenuList()
	{
		return this.menuList;
	}
	
	public MenuFrameAbstract getMenu(int menuPosition)
	{
		return this.menuList.get(menuPosition);
	}
	
	public MenuFrameAbstract getMenu(String menuName)
	{
		return getMenu(findMenu(menuName));
	}
	
	public int getSize()
	{
		return menuList.size();
	}
	
	public boolean add(String menuName, String frameTitle, int width, int height, String menuType)
	{
		boolean isValid = false;
		MenuFrameAbstract menuFrame = processBuildMenuRequest(menuType, menuName, frameTitle, width, height);
					
		this.menuList.add(menuFrame);
		
		return isValid;
		
	}
	
	public void remove(int index)
	{
		if (index >= 0 && index < this.menuList.size())
		{
			this.menuList.remove(index);
		}	
		
	}
	
	public void removeAll()
	{
		this.menuList.clear();
	}
	
	public int findMenu(String menuName)
	{
		boolean isFound = false;
		int menuPosition = -1;
		int count = 0;
		
		while (count < this.menuList.size() && isFound != true)
		{ 
			if (this.menuList.get(count).getMenuName().compareTo(menuName) == 0)
			{
				isFound = true;
				menuPosition = count;
			}
			
			count++;
			
		}
		
		return menuPosition;
		
	}
	
	public MenuFrameAbstract processBuildMenuRequest(String menuType, String menuName, String frameTitle, int width, int height)
	{
		menuType = menuType.toLowerCase();
		
		MenuFrameAbstract menuFrame = null;
		
		switch (menuType)
		{
			case "main":
				menuFrame = new MainMenuFrame(menuName, frameTitle, width, height);
				
				break;
				
				
			case "instruction-sub":
				menuFrame = new InstructionSubMenu(menuName, frameTitle, width, height);
				
				break;
				
				
			case "register-sub":
				menuFrame = new RegistersSubMenu(menuName, frameTitle, width, height);
				
				break;
					
				
			default:
			
		}
		
		return menuFrame;
	}
	
}