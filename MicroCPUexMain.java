

/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-09-17
 * 
 */

public class MicroCPUexMain
{
	public static void main(String[] args)
	{				
		MicroCPUex microCPUex = new MicroCPUex();
		microCPUex.launch();
		
	}
}