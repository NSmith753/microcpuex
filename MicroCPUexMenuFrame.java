

/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-09-21
 * 
 */

public abstract class MicroCPUexMenuFrame extends MenuFrameAbstract
{
	private static MicroCPUexCore microCPUexCore = new MicroCPUexCore();
	private static MicroCPUexGUI microCPUexGUI = new MicroCPUexGUI();
	
	
	public MicroCPUexMenuFrame()
	{
		super();
	}
	
	public MicroCPUexMenuFrame(String menuName, String frameTitle, int width, int height)
	{
		super(menuName, frameTitle, width, height);
	}
	
	public MicroCPUexCore getMicroCPUexCore()
	{
		return microCPUexCore;
	}
	
	public void setMicroCPUexCore(MicroCPUexCore microCPUexCore)
	{
		this.microCPUexCore = microCPUexCore;
	}
	
	public MicroCPUexGUI getMicroCPUexGUI()
	{
		return this.microCPUexGUI;
	}
	
	public void setMicroCPUexGUI(MicroCPUexGUI microCPUexGUI)
	{
		this.microCPUexGUI = microCPUexGUI;
	}
	
	public void buildGUI()
	{
		microCPUexGUI.add("main", "microCPUex", 400, 600, "main");
		microCPUexGUI.getMenu("main").build();
		
		microCPUexGUI.add("instruction-sub", "microCPUex", 400, 600, "instruction-sub");
		microCPUexGUI.getMenu("instruction-sub").build();
		
		microCPUexGUI.add("register-sub", "microCPUex", 400, 600, "register-sub");
		microCPUexGUI.getMenu("register-sub").build();
		
	}
	
}