# README #

### What is this repository for? ###

A program for a computer that has 10 registers and contains a memory of 1000 words. Each register and memory location can hold a value of natural number >=0 and <=999. The computer can process an instruction that consists of 3-digit value stored in a memory location. It is assumed that these instructions will be not be stored in the memory but processed one after another. Following scheme is used to create or execute an instruction:

|Instruction code |Description |
| ----------------| -----------|
|1xx |means halt execution. You can ignore the value of x |
|2dn |means value of register d = n |
|3dn |means value of register d = value of register d + n | 
|4dn |means value of register d = value of register d * n |
|5dn |means value of register d = value of register d / n |
|6ds |means value of register d = value of register s |
|7ds |means value of register d = value of register d + value of register s |
|8dx |means value of register d = sqrt (value of register d). You can ignore the value of x |
|9ds |means value of register d = (value of register d) S |
|0xx |System cannot process any instruction that is in the form of ‘0xx’. Show an error like following: |
|Output: |Instruction code ‘065’ is not supported by the system. |


Each register is initialized to 000. The contents of the memory are taken from the input and executed one after another. The result is always saved in the reduced format using modulus of 1000. The program also uses a Java Swing GUI to make it more user friendly.

* Version
v1.0.0.0


### How do I get set up? ###

Download a copy, cd to the directory in terminal, compile using ```javac *.java ``` and then run using the ```java MicroCPUexMain ``` command
Built for Java SE 8