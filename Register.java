

/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-09-12
 * 
 */

public class Register
{
	private String id = "";
	private int value = 0;
	private int minValue = 0;
	private int maxValue = 0;
	
	
	public Register()
	{
		this.id = "";
		this.value = 0;
		this.minValue = 0;
		this.maxValue = 0;
	}
	
	public Register(String id, int value, int minValue, int maxValue)
	{
		this.id = id;
		this.value = value;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}
	
	public void setID(String id)
	{
		this.id = id;
	}
	
	public void setValue(int value)
	{
		this.value = value;
	}
	
	public void setMinValue(int minValue)
	{
		this.minValue = minValue;
	}
	
	public void setMaxValue(int maxValue)
	{
		this.maxValue = maxValue;
	}
	
	
	public String getID()
	{
		return this.id;
	}
	
	public int getValue()
	{
		return this.value;
	}
	
	public int getMinValue()
	{
		return this.minValue;
	}
	
	public int getMaxValue()
	{
		return this.maxValue;
	}		
	
	public boolean checkUpdateValue(int value)
	{
		boolean isValid = false;
			
		
		if (value >= this.minValue && value <= this.maxValue)
		{
			isValid = true;
		}
		
		return isValid;
	}
	
	public boolean isValidRegisterValue()
	{		
		return checkUpdateValue(this.value);
	}
			
}