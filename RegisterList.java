import java.util.*;



/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.2.1
 * @since		2015-09-12
 * 
 */

public class RegisterList
{
	private List<Register> registerList = new ArrayList<Register>();
	
	
	public void setRegisterList(List<Register> registerList)
	{
		this.registerList = registerList;
	}
	
	public List<Register> getRegisterList()
	{
		return this.registerList;
	}
	
	public Register getRegister(int registerPosition)
	{
		return this.registerList.get(registerPosition);
	}
	
	public int getSize()
	{
		return registerList.size();
	}
	
	public boolean add(String registerId, int registerValue, int registerMinValue, int registerMaxValue)
	{
		boolean isValid = false;
		Register register = new Register(registerId, registerValue, registerMinValue, registerMaxValue);
		
		
		if (register.isValidRegisterValue())
		{
			isValid = true;
			this.registerList.add(register);
		}		
		
		return isValid;
		
	}
	
	public void remove(int index)
	{
		if (index >= 0 && index < this.registerList.size())
		{
			this.registerList.remove(index);
		}	
		
	}
	
	public void removeAll()
	{
		this.registerList.clear();
	}
	
	public int findRegister(String registerID)
	{
		boolean isFound = false;
		int registerPosition = -1;
		int count = 0;
		
		while (count < this.registerList.size() && isFound != true)
		{ 
			if (this.registerList.get(count).getID().compareTo(registerID) == 0)
			{
				isFound = true;
				registerPosition = count;
			}
			
			count++;
			
		}
		
		return registerPosition;
		
	}
	
	public boolean updateRegister(String registerID, int value)
	{
		boolean isUpdated = false;
		int registerPosition = findRegister(registerID);
						
		if (registerPosition > -1)
		{			
			if (registerList.get(registerPosition).checkUpdateValue(value))
			{				
				isUpdated = true;
				registerList.get(registerPosition).setValue(value);
			}
		}
				
		return isUpdated;
	}
	
	public int getRegisterValue(String registerID)
	{
		return registerList.get(findRegister(registerID)).getValue();
	}
	
	public boolean isValidRegisterValue(String registerID)
	{
		boolean isValid = false;
		int registerPosition = findRegister(registerID);
		int maxValue = 0;
		int minValue = 0;
		int value = 0;
		
		if (registerPosition > -1)
		{
			minValue = registerList.get(registerPosition).getMinValue();
			maxValue = registerList.get(registerPosition).getMaxValue();
			value = registerList.get(registerPosition).getValue();
			
			if (value >= minValue && value <= maxValue)
			{
				isValid = true;
			}
			
		}
		
		
		return isValid;
		
	}
	
}