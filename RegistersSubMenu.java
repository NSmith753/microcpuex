import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-09-19
 * 
 */

public class RegistersSubMenu extends MicroCPUexMenuFrame
{
	public RegistersSubMenu()
	{
		super();
	}
	
	public RegistersSubMenu(String menuName, String frameTitle, int width, int height)
	{
		super(menuName, frameTitle, width, height);
	}	
			
	public void build()
	{
		getHeaderLabel().setText("View the current state of the register list");
		getControlsLabel().setText("Please select an action");
		
		JButton viewAllRegistersButton = new JButton("View All Registers");
		JButton clearAllRegistersButton = new JButton("Clear All Registers");
		JButton backButton = new JButton("Back");
		
		viewAllRegistersButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getMicroCPUexCore().display();
				
			}
		});
		
		clearAllRegistersButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getMicroCPUexCore().clearRegisters();
				
			}
		});
		
		backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				getMicroCPUexGUI().getMenu("main").show();				
				close();
				
			}
		});
		
				
		
		getControlPanel().add(viewAllRegistersButton);
		getControlPanel().add(clearAllRegistersButton);
		getControlPanel().add(backButton);
				
	}
}